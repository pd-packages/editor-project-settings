# Changelog

---

## [v1.0.0](https://gitlab.com/pd-packages/editor-project-settings/-/tags/v1.0.0)

### Added

- ProjectSettingsEditor

---

## [v0.0.0](https://gitlab.com/pd-packages/editor-project-settings/-/tags/v0.0.0)

### Added

- For new features.

### Changed

- For changes in existing functionality.

### Deprecated

- For soon-to-be removed features.

### Removed

- For now removed features.

### Fixed

- For any bug fixes.

### Security

- In case of vulnerabilities.
