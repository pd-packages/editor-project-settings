using System;
using System.IO;
using UnityEngine;

namespace Playdarium.ProjectSettings
{
    public class ProjectSettingsEditor<TSettings>
        where TSettings : class, new()
    {
        private readonly Func<TSettings> _createDefault;
        private readonly string _fileName;

        public ProjectSettingsEditor(string fileName) : this(fileName, () => new TSettings())
        {
        }

        public ProjectSettingsEditor(string fileName, Func<TSettings> createDefault)
        {
            _createDefault = createDefault;
            _fileName = $"{fileName}.json";
        }

        private string SettingsPath
        {
            get
            {
                var projectPath = new DirectoryInfo(Application.dataPath).Parent.ToString();
                return Path.Combine(projectPath, "ProjectSettings", _fileName);
            }
        }

        public TSettings Read() => File.Exists(SettingsPath)
            ? JsonUtility.FromJson<TSettings>(File.ReadAllText(SettingsPath))
            : Save(_createDefault());

        public void Change(Action<TSettings> action)
        {
            var settings = Read();
            action(settings);
            Save(settings);
        }

        private TSettings Save(TSettings settings)
        {
            File.WriteAllText(SettingsPath, JsonUtility.ToJson(settings, true));
            return settings;
        }
    }
}